<?php

class EventsController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('admin.filter.AuthFilter'),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update'),
				'users'=>array(Yii::app()->user->name),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Events;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Events']))
		{
			$model->attributes=$_POST['Events'];
			
			$model->tgl_input = date("Y-m-d H:i:s");

			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->status = 1;
					$model->save();
					Log::createLog("EventsController Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$modelAttributes2 = array();
		// $modelAttributes = EventPart::model()->findAll('events_id = :id ORDER BY id asc', array(':id'=>$model->id));

		$modelAttributes2 = Yii::app()->db->createCommand('
SELECT a.*
FROM `tb_events_form` a 
where id not in (select form_id from tb_event_part where events_id = '.$model->id.')
order by a.id asc')->queryAll();

		$modelFrm2 = array();
		// $modelFrm2 = EventsForm::model()->findAll();

		$modelFrm2 = Yii::app()->db->createCommand('
SELECT a.*, b.events_id, b.form_id, b.active 
FROM `tb_events_form` a 
left join tb_event_part b 
on b.form_id = a.id 
where b.events_id = '.$model->id.'
order by a.id asc')->queryAll();

		unset($modelAttributes);
		if (is_array($_POST['EventPart']['form_id']) && count($_POST['EventPart']['form_id']) > 0) {
			foreach ($_POST['EventPart']['form_id'] as $key => $value) {
				$modelAttributes[$key] = new EventPart;
				if ($value != '') {
					$modelAttributes[$key]->events_id = $model->id;
					$modelAttributes[$key]->active = $_POST['EventPart']['active'][$key];
					$modelAttributes[$key]->form_id = $value;
				}
				
			}
		}

		if(isset($_POST['Events']))
		{
			$model->attributes=$_POST['Events'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();

					EventPart::model()->deleteAll('events_id = :id', array(':id'=>$model->id));
					if (count($_POST['EventPart']['form_id']) > 0) {
						foreach ($_POST['EventPart']['form_id'] as $key => $value) {
							$modelAttributes[$key] = new EventPart;
							if ($value != '') {
								$modelAttributes[$key]->events_id = $model->id;
								$modelAttributes[$key]->active = $_POST['EventPart']['active'][$key];
								$modelAttributes[$key]->form_id = $value;

								$modelAttributes[$key]->save(false);
							}
							
						}
					}

					Log::createLog("EventsController Update $model->id");
					Yii::app()->user->setFlash('success','Data Edited');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		// $modelFrm2 = EventPart::model()->findAll('events_id = :id ORDER BY id asc', array(':id'=>$model->id));

		$this->render('update',array(
			'model'=>$model,
			'modelAttributes'=>$modelAttributes2,
			'modelFrm2'=>$modelFrm2,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			$this->loadModel($id)->delete();

			// if(!isset($_GET['ajax']))
				$this->redirect( array('index') );
		// }
		// else
		// 	throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Events('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Events']))
			$model->attributes=$_GET['Events'];

		$dataProvider=new CActiveDataProvider('Events');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Events::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='events-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
