<?php

class EventsRegController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('admin.filter.AuthFilter'),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'users'=>array(Yii::app()->user->name),
				'actions'=>array('delete','index','view','create','update', 'urut_comb'),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new EventsReg;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EventsReg']))
		{
			$model->attributes=$_POST['EventsReg'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();
					Log::createLog("EventsRegController Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('index', 'event_id'=> $model->event_id));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EventsReg']))
		{
			$model->attributes=$_POST['EventsReg'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					if (isset($model->datas)) {
						$model->datas = serialize($model->datas);
					}

					$model->save();
					Log::createLog("EventsRegController Update $model->id");
					Yii::app()->user->setFlash('success','Data Edited');
				    $transaction->commit();
					$this->redirect(array('index', 'event_id'=> $model->event_id));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$model->hadir_sesi_1 = ($model->hadir_sesi_1 == 1)? 'Ya':'';
		$model->hadir_sesi_2 = ($model->hadir_sesi_2 == 1)? 'Ya':'';
		$model->hadir_sesi_3 = ($model->hadir_sesi_3 == 1)? 'Ya':'';

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			// we only allow deletion via POST request
			$parents = $this->loadModel($id);
			$ev_id = $parents->id;
			$parents->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			// if(!isset($_GET['ajax']))
			$this->redirect( array('index', 'event_id'=> $model->event_id));
		// }
		// else
		// 	throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new EventsReg('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EventsReg']))
			$model->attributes=$_GET['EventsReg'];

		$model->event_id = $_GET['event_id'];

		$dataProvider=new CActiveDataProvider('EventsReg');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	public function actionDownExcel()
	{
		$even_id = $_GET['event_id'];		
		$data = EventsReg::model()->findAll('event_id = :evn_id', array(':evn_id'=> $even_id));

		$this->layout = '//layoutsAdmin/mainKosong';

		$this->render('down_excell',array(
			'model'=>$data,
		));
	}

	public function actionUrut_comb()
	{
		echo "dones";
		exit;
		$bloc_resource = Events::model()->findAll();
		foreach ($bloc_resource as $ke => $val) {
			$resource_mod_{$ke} = EventsReg::model()->findAll('event_id = :evn_id order by t.id ASC', array(':evn_id'=> $val->id));
			
			$noms_{$ke} = 1;
			foreach ($resource_mod_{$ke} as $key => $value) {
				$data = EventsReg::model()->findByPk($value->id);
				$n2 = str_pad($noms_{$ke}, 4, 0, STR_PAD_LEFT);
				$data->nomer_peserta = $n2;
				$data->save(false);
				$noms_{$ke}++;
			}
		}

		echo "data hasbeen edited";
		exit;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=EventsReg::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='events-reg-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
