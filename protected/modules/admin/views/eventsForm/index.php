<?php
$this->breadcrumbs=array(
	'Events Forms',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Events Form',
	'subtitle'=>'Data Events Form',
);

$this->menu=array(
	array('label'=>'Add Events Form', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>
<?php endif; ?>

<h1>Events Form</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'events-form-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		// 'name',
		'label',
		// 'types',
		// 'datas',
		array(
			'header' => 'Lock',
			'type' => 'raw',
			'value' => '($data->locks == 1)? "Lock": "Normal"',
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
