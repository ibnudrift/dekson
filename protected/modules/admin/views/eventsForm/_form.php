<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'events-form-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data EventsForm</h4>
<div class="widgetcontent">
	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>225, 'hint'=>'*) Tidak boleh spasi / tanda -, hanya character abjad', 'required'=>'required')); ?>

	<?php echo $form->textFieldRow($model,'label',array('class'=>'span5','maxlength'=>225)); ?>

	<?php // echo $form->dropDownListRow($model,'types', array('text'=>'text', 'textarea'=>'textarea', 'datepicker'=>'datepicker', 'dropdown'=>'dropdown'), array('class'=>'span5', 'hint'=>'*) text, textarea')); ?>

	<?php // echo $form->textAreaRow($model,'datas',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
