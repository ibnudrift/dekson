<?php
$this->breadcrumbs=array(
	'Events Forms'=>array('index'),
	// $model->name=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Events Form',
	'subtitle'=>'Edit Events Form',
);

$this->menu=array(
	array('label'=>'List Events Form', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Events Form', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Events Form', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>