<?php
$this->breadcrumbs=array(
	'Events Forms'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List EventsForm','url'=>array('index')),
	array('label'=>'Add EventsForm','url'=>array('create')),
);
?>

<h1>Manage Events Forms</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'events-form-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'label',
		'types',
		'datas',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
