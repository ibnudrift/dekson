<?php
$this->breadcrumbs=array(
	'Events Forms'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Events Form',
	'subtitle'=>'Add Events Form',
);

$this->menu=array(
	array('label'=>'List Events Form', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>