<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'tgl_lahir',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textAreaRow($model,'address',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'company',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textAreaRow($model,'notes',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'register_code',array('class'=>'span5','maxlength'=>2225)); ?>

	<?php echo $form->textFieldRow($model,'url_qrcode',array('class'=>'span5','maxlength'=>2225)); ?>

	<?php echo $form->textFieldRow($model,'hadir_sesi_1',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'hadir_sesi_2',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'hadir_sesi_3',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'event_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'event_name',array('class'=>'span5','maxlength'=>225)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
