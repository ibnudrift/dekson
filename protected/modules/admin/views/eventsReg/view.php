<?php
$this->breadcrumbs=array(
	'Events Regs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EventsReg', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add EventsReg', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit EventsReg', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete EventsReg', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View EventsReg #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tgl_lahir',
		'phone',
		'email',
		'address',
		'company',
		'notes',
		'register_code',
		'url_qrcode',
		'hadir_sesi_1',
		'hadir_sesi_2',
		'hadir_sesi_3',
		'event_id',
		'event_name',
	),
)); ?>
