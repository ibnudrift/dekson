<?php
$this->breadcrumbs=array(
	'Events Regs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List EventsReg','url'=>array('index')),
	array('label'=>'Add EventsReg','url'=>array('create')),
);
?>

<h1>Manage Events Regs</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'events-reg-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'tgl_lahir',
		'phone',
		'email',
		'address',
		'company',
		/*
		'notes',
		'register_code',
		'url_qrcode',
		'hadir_sesi_1',
		'hadir_sesi_2',
		'hadir_sesi_3',
		'event_id',
		'event_name',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
