<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'events-reg-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data EventsReg</h4>
<div class="widgetcontent">


	<?php echo $form->textFieldRow($model,'tgl_lahir',array('class'=>'span5')); ?>
	<?php echo $form->textFieldRow($model,'usia',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textAreaRow($model,'address',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'company',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'instansi',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'kodepos',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'nomer_peserta',array('class'=>'span5','maxlength'=>225, 'readonly'=>'readonly')); ?>
	
	<?php if (isset($model->hobi_lain) && $model->hobi_lain != ''):
		$model->hobi = $model->hobi_lain;
	endif ?>

	<?php echo $form->textFieldRow($model,'hobi',array('class'=>'span5','maxlength'=>225)); ?>
	
	<?php if (isset($model->datas)): 
	$datasn = unserialize($model->datas); ?>
	<?php foreach ($datasn as $key => $value): ?>
	<div class="control-group "><label class="control-label" for="EventsReg_register_code"><?php echo ucwords($key) ?></label><div class="controls"><input class="span5" maxlength="2225" name="EventsReg[datas][<?php echo $key ?>]" id="EventsReg_register_code" type="text" value="<?php echo $value ?>"></div></div>
	<?php endforeach ?>
	<?php endif ?>

	<?php // echo $form->textAreaRow($model,'notes',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'register_code',array('class'=>'span5','maxlength'=>2225)); ?>

	<?php // echo $form->textFieldRow($model,'url_qrcode',array('class'=>'span5','maxlength'=>2225)); ?>
	<div class="control-group">
		<label class="control-label" for="MassReg_url_qrcode">URL QrCode</label>
		<div class="controls">
			<div class="thumbnail_qr">
				<img src="<?php echo $model->url_qrcode ?>" alt="" style="max-width: 140px;">
			</div>
			<a target="_blank" href="<?php echo $model->url_qrcode ?>" class="btn btn-link">Link QrCode</a>
		</div>
	</div>

	<?php echo $form->textFieldRow($model,'hadir_sesi_1',array('class'=>'span5', 'disabled'=>'disabled')); ?>

	<?php echo $form->textFieldRow($model,'hadir_sesi_2',array('class'=>'span5', 'disabled'=>'disabled')); ?>

	<?php echo $form->textFieldRow($model,'hadir_sesi_3',array('class'=>'span5', 'disabled'=>'disabled')); ?>

	<?php // echo $form->textFieldRow($model,'event_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'event_name',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php 
	
	// $this->widget('bootstrap.widgets.TbButton', array(
		


	?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		// 'buttonType'=>'submit',
		// 'type'=>'info',
		'url'=>CHtml::normalizeUrl(array('index', 'event_id'=> $model->event_id)),
		'label'=>'Batal',
	)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>

<style type="text/css">
	.thumbnail_qr img{
		border: 5px solid #ccc;
		padding: 0.3rem;
	}
</style>