<?php
$this->breadcrumbs=array(
	'Events Regs'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'EventsReg',
	'subtitle'=>'Add EventsReg',
);

$this->menu=array(
	array('label'=>'List EventsReg', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>