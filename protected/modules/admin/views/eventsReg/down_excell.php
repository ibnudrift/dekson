<!DOCTYPE html>
<html>
<head>
	<title>Export Excell - Dekkson</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;

	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>

	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=data_peserta.xls");
	?>

	<table border="1"> 
		<tr>
			<th>Name</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Company</th>
			<th>Tanggal Lahir</th>
			<th>Alamat</th>
			<th>Asosiasi / Instansi</th>
			<th>Kodepos</th> 
			<th>Sesi 1</th> 
			<th>Sesi 2</th> 
			<th>Sesi 3</th> 
			<th>Nomer Peserta</th> 
		</tr> 
		<?php foreach ($model as $key => $value): ?>
		<tr>
			<td><?php echo $value->name; ?></td>
			<td><?php echo $value->phone; ?></td>
			<td><?php echo $value->email; ?></td>
			<td><?php echo $value->company; ?></td>
			<td><?php echo $value->tgl_lahir; ?></td>
			<td><?php echo $value->address; ?></td>
			<td><?php echo $value->instansi; ?></td>
			<td><?php echo $value->kodepos; ?></td>
			<td><?php echo (intval($value->hadir_sesi_1) != 1)? "Tidak Hadir": "Hadir"; ?></td>
			<td><?php echo (intval($value->hadir_sesi_2) != 1)? "Tidak Hadir": "Hadir"; ?></td>
			<td><?php echo (intval($value->hadir_sesi_3) != 1)? "Tidak Hadir": "Hadir"; ?></td>
			<td><?php echo $value->nomer_peserta; ?></td>
		</tr>
		<?php endforeach ?>
	</table>
</body>
</html>