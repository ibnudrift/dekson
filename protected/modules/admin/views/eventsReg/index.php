<?php
$this->breadcrumbs=array(
	'Events Register List',
);

$n_parent = Events::model()->findByPk($_GET['event_id']);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Events Register',
	'subtitle'=>'Data Events Register',
	'subtitle'=>'Data Mass Register List > <small>'. ucwords(strtolower($n_parent->name)) .' - '. $n_parent->tgl_event.'</small>',
);

$this->menu=array(
	array('label'=>'Download Excell', 'icon'=>'','url'=>array('downExcel', 'event_id'=> $_GET['event_id'])),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->textFieldRow($model,'name',array('class'=>'span12','maxlength'=>200, 'placeholder'=>'Search  Name')); ?>
		</div>
		<div class="span4">
			<?php echo $form->textFieldRow($model,'email',array('class'=>'span12','maxlength'=>200, 'placeholder'=>'Search Email')); ?>
			<?php 
			if (isset($_GET['event_id'])) {				
				$model->event_id = $_GET['event_id'];
			}else{
				$model->event_id = $_GET['EventsReg']['event_id'];
			}
			?>
			<?php echo $form->hiddenField($model,'event_id', array('class'=>'span12')); ?>
		</div>
	</div>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>'Search',
	)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		// 'buttonType'=>'button',
		'type'=>'primary',
		'label'=>'Reset',
		'url'=>Yii::app()->createUrl($this->route),
	)); ?>

<?php $this->endWidget(); ?>

<h1>Events Register</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'events-reg-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		'nomer_peserta',
		'name',
		'phone',
		'email',
		array(
			'header'=>'Kehadiran Status',
			'type'=>'raw',
			'value'=>'($data->hadir_sesi_1 == 1 && $data->hadir_sesi_2 == 1 && $data->hadir_sesi_3 == 1)? "LULUS": "Tidak Lulus"',
		),
		'company',
		'date_input',
		/*
		'register_code',
		'url_qrcode',
		'event_id',
		'event_name',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
