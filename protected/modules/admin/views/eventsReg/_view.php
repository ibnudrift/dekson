<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company')); ?>:</b>
	<?php echo CHtml::encode($data->company); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('register_code')); ?>:</b>
	<?php echo CHtml::encode($data->register_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url_qrcode')); ?>:</b>
	<?php echo CHtml::encode($data->url_qrcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hadir_sesi_1')); ?>:</b>
	<?php echo CHtml::encode($data->hadir_sesi_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hadir_sesi_2')); ?>:</b>
	<?php echo CHtml::encode($data->hadir_sesi_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hadir_sesi_3')); ?>:</b>
	<?php echo CHtml::encode($data->hadir_sesi_3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('event_id')); ?>:</b>
	<?php echo CHtml::encode($data->event_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('event_name')); ?>:</b>
	<?php echo CHtml::encode($data->event_name); ?>
	<br />

	*/ ?>

</div>