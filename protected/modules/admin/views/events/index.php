<?php
$this->breadcrumbs=array(
	'Events',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Events',
	'subtitle'=>'Data Events',
);

$this->menu=array(
	array('label'=>'Add Event', 'icon'=>'plus-sign', 'url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>
<?php endif; ?>

<h1>Events</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'events-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		// 'tgl_input',
		'tgl_event',
		'name',
		'location',
		array(
			'header'=>'Status',
			'type'=>'raw',
			'value'=>'($data->status != 1)? "Close": "Open"',
		),
		array(
			'header'=>'Peserta',
			'type'=>'raw',
			'value'=>'EventsReg::model()->totals_peserta($data->id)',
		),
		'kode_verify',
		/*
		'description',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			// 'template'=>'{update} &nbsp; {delete}',
			'template'=>'{update} &nbsp; {delete} &nbsp; {view_list}',
			'buttons'=>array(
				'view_list' => array(
				    'label'=>'<i class="fa fa-arrow-right"></i>',  
				    'url'=>'CHtml::normalizeUrl(array("/admin/eventsReg/index", "event_id" => $data->id))',
				),
			),
		),
	),
)); ?>
