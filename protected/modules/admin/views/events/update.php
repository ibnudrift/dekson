<?php
$this->breadcrumbs=array(
	'Events'=>array('index'),
	// $model->name=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Events',
	'subtitle'=>'Edit Events',
);

$this->menu=array(
	array('label'=>'List Events', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Events', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Events', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelAttributes'=> $modelAttributes, 'modelFrm2'=> $modelFrm2)); ?>