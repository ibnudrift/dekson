<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'events-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row-fluid">
	<div class="span8">
		<div class="widget">
			<h4 class="widgettitle">Data Events</h4>
			<div class="widgetcontent">

				<?php echo $form->textFieldRow($model,'tgl_event',array('class'=>'span5 datepicker2')); ?>

				<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>225)); ?>

				<?php echo $form->textFieldRow($model,'location',array('class'=>'span5','maxlength'=>225)); ?>

				<?php echo $form->textAreaRow($model,'description', array('rows'=>3, 'class'=>'span8', 'placeholder'=>'Ex. Price of Seminar Event and other...')); ?>

				<?php echo $form->textFieldRow($model,'kode_verify',array('class'=>'span5','maxlength'=>4, 'minlength'=>4, 'required'=>'required')); ?>

				<?php echo $form->textFieldRow($model,'event_phone',array('class'=>'span5','maxlength'=>225)); ?>
				<?php echo $form->textFieldRow($model,'event_email',array('class'=>'span5','maxlength'=>225)); ?>
				
				<?php echo $form->textAreaRow($model,'content_sucess', array('rows'=>3, 'class'=>'span8', 'placeholder'=>'Success Content Page')); ?>
				<?php echo $form->textAreaRow($model,'content_email', array('rows'=>3, 'class'=>'span8', 'placeholder'=>'Success Content at Email sent Event')); ?>

				<?php echo $form->dropDownListRow($model, 'status', array(
				        		'1'=>'Active Event',
				        		'0'=>'Close Event',
				        	), array('class'=>'span5')); ?>
			</div>
		</div>
	</div>
	<div class="span4">
		<div class="widget">
			<h4 class="widgettitle">Actions</h4>
			<div class="widgetcontent">
				<?php $this->widget('bootstrap.widgets.TbButton', array(
						'buttonType'=>'submit',
						'type'=>'primary',
						'label'=>$model->isNewRecord ? 'Add' : 'Save',
					)); ?>
					<?php $this->widget('bootstrap.widgets.TbButton', array(
						// 'buttonType'=>'submit',
						// 'type'=>'info',
						'url'=>CHtml::normalizeUrl(array('index')),
						'label'=>'Batal',
					)); ?>
			</div>
		</div>

		<div class="widget">
			<h4 class="widgettitle">List Form Participants</h4>
			<div class="widgetcontent">
				<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody class="option-tempel">
						<?php if (is_array($modelFrm2) && count($modelFrm2) > 0): ?>
							<?php foreach ($modelFrm2 as $key => $value): ?>
	                        <tr>
	                            <td>
	                            	<input type="hidden" name="EventPart[events_id][]" value="<?php echo $model['id'] ?>">
	                            	<input type="hidden" name="EventPart[form_id][]" value="<?php echo $value['id'] ?>">
	                            	<input type="text" name="EventPart[form_name][]" readonly="readonly" value="<?php echo $value['name'] ?>" class="input-block-level">
	                            </td>
	                            </td>
	                            <td>
	                            	<select name="EventPart[active][]" id="" class="input-block-level" required="required">
	                            		<option value="">--Pilih--</option>
	                            		<option <?php if($value['active'] == 0): ?>selected="selected"<?php endif ?> value="0">Non Aktif</option>
	                            		<option <?php if($value['active'] == 1): ?>selected="selected"<?php endif ?> value="1">Aktif</option>
	                            	</select>
	                            </td>
	                        </tr>
	                    	<?php endforeach ?>
						<?php endif ?>

						<?php if (is_array($modelAttributes) && count($modelAttributes) > 0): ?>
							<?php foreach ($modelAttributes as $key => $value): ?>
	                        <tr>
	                            <td>
	                            	<input type="hidden" name="EventPart[events_id][]" value="<?php echo $model['id'] ?>">
	                            	<input type="hidden" name="EventPart[form_id][]" value="<?php echo $value['id'] ?>">
	                            	<input type="text" name="EventPart[form_name][]" readonly="readonly" value="<?php echo $value['name'] ?>" class="input-block-level">
	                            </td>
	                            </td>
	                            <td>
	                            	<select name="EventPart[active][]" id="" class="input-block-level" required="required">
	                            		<option value="">--Pilih--</option>
	                            		<option selected="selected" value="0">Non Aktif</option>
	                            		<option value="1">Aktif</option>
	                            	</select>
	                            </td>
	                        </tr>
	                    	<?php endforeach ?>
						<?php endif ?>
                    </tbody>
				</table>
				<div class="clear"></div>
			</div>
		</div>

	</div>
</div>

<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
