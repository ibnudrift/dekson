<?php

/**
 * This is the model class for table "tb_event_reg".
 *
 * The followings are the available columns in table 'tb_event_reg':
 * @property string $id
 * @property string $tgl_lahir
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $company
 * @property string $notes
 * @property string $register_code
 * @property string $url_qrcode
 * @property integer $hadir_sesi_1
 * @property integer $hadir_sesi_2
 * @property integer $hadir_sesi_3
 * @property integer $event_id
 * @property string $event_name
 */
class EventsReg extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EventsReg the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_event_reg';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('event_id', 'numerical', 'integerOnly'=>true),
			array('phone, email, company, event_name', 'length', 'max'=>225),
			array('register_code, url_qrcode', 'length', 'max'=>2225),
			array('tgl_lahir, address, notes, hadir_sesi_1, hadir_sesi_2, hadir_sesi_3, name, date_input, usia, company, kodepos, hobi, hobi_lain, instansi, datas', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tgl_lahir, phone, email, address, company, notes, register_code, url_qrcode, hadir_sesi_1, hadir_sesi_2, hadir_sesi_3, event_id, event_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tgl_lahir' => 'Tgl Lahir',
			'phone' => 'Phone',
			'email' => 'Email',
			'address' => 'Alamat',
			'company' => 'Perusahaan',
			'notes' => 'Notes',
			'register_code' => 'Register Code',
			'url_qrcode' => 'Url Qrcode',
			'hadir_sesi_1' => 'Hadir Sesi 1',
			'hadir_sesi_2' => 'Hadir Sesi 2',
			'hadir_sesi_3' => 'Hadir Sesi 3',
			'event_id' => 'Event',
			'date_input' => 'Date Input',
			'event_name' => 'Event Name',
			'kodepos' => 'Kode Pos',
			'instansi' => 'Asosiasi / Instansi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('tgl_lahir',$this->tgl_lahir,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('register_code',$this->register_code,true);
		$criteria->compare('url_qrcode',$this->url_qrcode,true);
		$criteria->compare('hadir_sesi_1',$this->hadir_sesi_1);
		$criteria->compare('hadir_sesi_2',$this->hadir_sesi_2);
		$criteria->compare('hadir_sesi_3',$this->hadir_sesi_3);
		
		$criteria->compare('event_id',$this->event_id, true);
		
		$criteria->compare('event_name',$this->event_name,true);
		$criteria->compare('kodepos',$this->kodepos,true);
		$criteria->compare('instansi',$this->instansi,true);
		$criteria->compare('nomer_peserta',$this->nomer_peserta,true);

		$criteria->order = 't.id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
		        'pageSize'=> 25,
		    ),
		));
	}

	public function totals_peserta($evn_id)
	{
		return count( EventsReg::model()->findAll('event_id = :ev_id', array(':ev_id'=> $evn_id)) );
	}

}