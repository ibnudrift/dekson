<?php

/**
 * This is the model class for table "tb_events_list".
 *
 * The followings are the available columns in table 'tb_events_list':
 * @property string $id
 * @property string $tgl_input
 * @property string $tgl_event
 * @property string $name
 * @property string $location
 * @property integer $status
 * @property string $description
 */
class Events extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Events the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_events_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('status', 'numerical', 'integerOnly'=>true),
			array('name, location', 'length', 'max'=>225),
			array('tgl_input, tgl_event, description, status, kode_verify, content_sucess, content_email, event_phone, event_email', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tgl_input, tgl_event, name, location, status, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tgl_input' => 'Tgl Input',
			'tgl_event' => 'Tgl Event',
			'name' => 'Name',
			'location' => 'Location',
			'description' => 'Description',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('tgl_input',$this->tgl_input,true);
		$criteria->compare('tgl_event',$this->tgl_event,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('description',$this->description,true);

		$criteria->order = 't.id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}