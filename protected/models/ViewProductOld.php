<?php

/**
 * This is the model class for table "view_product_old".
 *
 * The followings are the available columns in table 'view_product_old':
 * @property integer $id_produk
 * @property string $kode_produk
 * @property integer $kategori
 * @property integer $jenis_produk
 * @property integer $subkategori
 * @property integer $aksesoris
 * @property integer $bahan
 * @property integer $finishing
 * @property string $tag
 * @property string $nama_produk
 * @property string $permalink
 * @property integer $redirect
 * @property integer $jendela
 * @property integer $harga
 * @property integer $stok
 * @property integer $alert_stok
 * @property double $berat
 * @property integer $diskon
 * @property integer $hargacoret
 * @property string $variant
 * @property integer $alert_coret
 * @property string $deskripsi
 * @property string $deskripsi2
 * @property integer $dibeli
 * @property integer $dilihat
 * @property integer $rating
 * @property string $gambar_utama
 * @property string $gambar1
 * @property string $gambar2
 * @property string $gambar3
 * @property string $gambar4
 * @property integer $atribut
 * @property string $watermark
 * @property integer $alert_watermark
 * @property string $meta_title
 * @property string $meta_deskripsi
 * @property string $meta_keyword
 * @property string $tanggal
 * @property string $user
 * @property integer $tampilkan
 * @property string $parent
 * @property string $appproduk
 * @property integer $subkategorilv1
 * @property integer $subkategorilv2
 * @property string $aksesoris_name
 * @property string $bahan_name
 * @property string $color_name
 */
class ViewProductOld extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewProductOld the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'view_product_old';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_produk, aksesoris, bahan, finishing, subkategorilv1, subkategorilv2', 'required'),
			array('id_produk, kategori, jenis_produk, subkategori, aksesoris, bahan, finishing, redirect, jendela, harga, stok, alert_stok, diskon, hargacoret, alert_coret, dibeli, dilihat, rating, atribut, alert_watermark, tampilkan, subkategorilv1, subkategorilv2', 'numerical', 'integerOnly'=>true),
			array('berat', 'numerical'),
			array('kode_produk', 'length', 'max'=>10),
			array('tag, nama_produk, gambar_utama, gambar1, gambar2, gambar3, gambar4, watermark, user, parent, appproduk, aksesoris_name, bahan_name', 'length', 'max'=>100),
			array('permalink, variant, meta_title', 'length', 'max'=>200),
			array('deskripsi, deskripsi2, meta_deskripsi, meta_keyword, tanggal, color_name, finishing_name', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_produk, kode_produk, kategori, jenis_produk, subkategori, aksesoris, bahan, finishing, tag, nama_produk, permalink, redirect, jendela, harga, stok, alert_stok, berat, diskon, hargacoret, variant, alert_coret, deskripsi, deskripsi2, dibeli, dilihat, rating, gambar_utama, gambar1, gambar2, gambar3, gambar4, atribut, watermark, alert_watermark, meta_title, meta_deskripsi, meta_keyword, tanggal, user, tampilkan, parent, appproduk, subkategorilv1, subkategorilv2, aksesoris_name, bahan_name, color_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_produk' => 'Id Produk',
			'kode_produk' => 'Kode Produk',
			'kategori' => 'Kategori',
			'jenis_produk' => 'Jenis Produk',
			'subkategori' => 'Subkategori',
			'aksesoris' => 'Aksesoris',
			'bahan' => 'Bahan',
			'finishing' => 'Finishing',
			'tag' => 'Tag',
			'nama_produk' => 'Nama Produk',
			'permalink' => 'Permalink',
			'redirect' => 'Redirect',
			'jendela' => 'Jendela',
			'harga' => 'Harga',
			'stok' => 'Stok',
			'alert_stok' => 'Alert Stok',
			'berat' => 'Berat',
			'diskon' => 'Diskon',
			'hargacoret' => 'Hargacoret',
			'variant' => 'Variant',
			'alert_coret' => 'Alert Coret',
			'deskripsi' => 'Deskripsi',
			'deskripsi2' => 'Deskripsi2',
			'dibeli' => 'Dibeli',
			'dilihat' => 'Dilihat',
			'rating' => 'Rating',
			'gambar_utama' => 'Gambar Utama',
			'gambar1' => 'Gambar1',
			'gambar2' => 'Gambar2',
			'gambar3' => 'Gambar3',
			'gambar4' => 'Gambar4',
			'atribut' => 'Atribut',
			'watermark' => 'Watermark',
			'alert_watermark' => 'Alert Watermark',
			'meta_title' => 'Meta Title',
			'meta_deskripsi' => 'Meta Deskripsi',
			'meta_keyword' => 'Meta Keyword',
			'tanggal' => 'Tanggal',
			'user' => 'User',
			'tampilkan' => 'Tampilkan',
			'parent' => 'Parent',
			'appproduk' => 'Appproduk',
			'subkategorilv1' => 'Subkategorilv1',
			'subkategorilv2' => 'Subkategorilv2',
			'aksesoris_name' => 'Aksesoris Name',
			'bahan_name' => 'Bahan Name',
			'color_name' => 'Color Name',
			'finishing_name' => 'finishing name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_produk',$this->id_produk);
		$criteria->compare('kode_produk',$this->kode_produk,true);
		$criteria->compare('kategori',$this->kategori);
		$criteria->compare('jenis_produk',$this->jenis_produk);
		$criteria->compare('subkategori',$this->subkategori);
		$criteria->compare('aksesoris',$this->aksesoris);
		$criteria->compare('bahan',$this->bahan);
		$criteria->compare('finishing',$this->finishing);
		$criteria->compare('tag',$this->tag,true);
		$criteria->compare('nama_produk',$this->nama_produk,true);
		$criteria->compare('permalink',$this->permalink,true);
		$criteria->compare('redirect',$this->redirect);
		$criteria->compare('jendela',$this->jendela);
		$criteria->compare('harga',$this->harga);
		$criteria->compare('stok',$this->stok);
		$criteria->compare('alert_stok',$this->alert_stok);
		$criteria->compare('berat',$this->berat);
		$criteria->compare('diskon',$this->diskon);
		$criteria->compare('hargacoret',$this->hargacoret);
		$criteria->compare('variant',$this->variant,true);
		$criteria->compare('alert_coret',$this->alert_coret);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('deskripsi2',$this->deskripsi2,true);
		$criteria->compare('dibeli',$this->dibeli);
		$criteria->compare('dilihat',$this->dilihat);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('gambar_utama',$this->gambar_utama,true);
		$criteria->compare('gambar1',$this->gambar1,true);
		$criteria->compare('gambar2',$this->gambar2,true);
		$criteria->compare('gambar3',$this->gambar3,true);
		$criteria->compare('gambar4',$this->gambar4,true);
		$criteria->compare('atribut',$this->atribut);
		$criteria->compare('watermark',$this->watermark,true);
		$criteria->compare('alert_watermark',$this->alert_watermark);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_deskripsi',$this->meta_deskripsi,true);
		$criteria->compare('meta_keyword',$this->meta_keyword,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('tampilkan',$this->tampilkan);
		$criteria->compare('parent',$this->parent,true);
		$criteria->compare('appproduk',$this->appproduk,true);
		$criteria->compare('subkategorilv1',$this->subkategorilv1);
		$criteria->compare('subkategorilv2',$this->subkategorilv2);
		$criteria->compare('aksesoris_name',$this->aksesoris_name,true);
		$criteria->compare('bahan_name',$this->bahan_name,true);
		$criteria->compare('color_name',$this->color_name,true);
		$criteria->compare('finishing_name',$this->finishing_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}