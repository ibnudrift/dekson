<?php

class EventController extends Controller
{
	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}	

	public function actionIndex()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Event Registration '. $this->pageTitle;

		$criteria = new CdbCriteria;
		$criteria->order = 't.id DESC';
		$model = Events::model()->findAll($criteria);
		
		$this->render('index', array(
			'data' => $model,
		));
	}

	public function actionDetail()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Event Registration '. $this->pageTitle;

		$nom_id = intval($_GET['id']);
		$data = Events::model()->findByPk($nom_id);

		$model = new EventsReg;
		$model->scenario = 'insert';

		$frm_active = Yii::app()->db->createCommand('
			SELECT a.*, b.active 
			FROM tb_events_form a 
			right join tb_event_part b on a.id = b.form_id 
			where b.events_id = '.$nom_id.' and b.active = 1 order by id asc')
		->queryAll();

		if(isset($_POST['EventsReg']))
		{
			$model->attributes=$_POST['EventsReg'];

	        $secret_key = "6Le4sLQZAAAAAJXwWC8T36agK-9vnCMz_oZlsD-Z";
	        $ch = curl_init("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);   
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		$response = curl_exec($ch);
    		curl_close($ch);

	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          	$model->addError('verifyCode','Pastikan anda sudah menyelesaikan captcha');
	        }else{

	        	// check duplicate
	        	$f_member = EventsReg::model()->find('email = :email and event_id = :evn_id', array(':email'=>$model->email, ':evn_id'=> $model->event_id));
	        	if ( $f_member !== null ) {
	        		$model->addError('email', 'Sorry, You are has been registered.');
	        	}

	        	// Get umur
	        	$ages = 0;
	        	if (isset($model->tgl_lahir)) {
					$from = new DateTime($model->tgl_lahir);
					$to   = new DateTime('today');
					$ages = $from->diff($to)->y;
	        	}

				if(!$model->hasErrors() && $model->validate())
				{
					$seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // and any other characters
					shuffle($seed); // probably optional since array_is randomized; this may be redundant
					$rand = '';
					foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];

					$model->register_code = 'DKSN-'.$model->event_id.'-'.date('Ym').'-'.$rand.'-'.rand(50, 1000);
					$model->date_input = date("Y-m-d H:i:s");

					$str_qr_gen = 'https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=';
					$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
					$model->url_qrcode = $str_qr_gen . $baseUrl . '/event/seminar/?kode=' . $model->register_code;

					// get event data
					$q_event = Events::model()->findByPk($model->event_id);
					$model->event_id = $model->event_id;
					$model->event_name = $q_event->name;
					$model->usia = $ages;
					$model->datas = isset($model->datas)? serialize($model->datas): null;

					// get nomer peserta
					$last_number = EventsReg::model()->find('t.event_id = :ev_id order by t.id desc', array(':ev_id'=> $model->event_id));
					$n2 = str_pad( ($last_number->nomer_peserta + 1), 4, 0, STR_PAD_LEFT);
					$model->nomer_peserta = $n2;

					$model->save(false);

					// config email
					$messaged = $this->renderPartial('//mail/events_schedule',array(
						'model'=>$model,
						'event_data' => $q_event,
						'frm_active' => $frm_active,
					),TRUE);

					$config = array(
						'to'=>array($model->email, $this->setting['email']),
						'bcc'=>array(),
						'subject'=>'(Dekkson) Event Registration for '.$model->email,
						'message'=>$messaged,
					);

					if ($this->setting['contact_cc']) {
						$config['cc'] = array($this->setting['contact_cc']);
					}
					if ($this->setting['contact_bcc']) { 
						$config['bcc'] = array($this->setting['contact_bcc']);
					}
					// kirim email 
					Common::mail($config);

					Yii::app()->user->setFlash('success','Thank you for Regitration. We will respond to you as soon as possible.');
					
					$this->redirect(array('/event/success_reg', 'id'=> $data->id, 'slug'=> $_GET['slug']));
				}
			}

		}
		
		$this->render('detail', array(
			'data' => $data,
			'model' => $model,
			'frm_active' => $frm_active,
		));
	}

	public function actionSuccess_reg()
	{
		$this->pageTitle = 'Success Registration - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$nom_id = intval($_GET['id']);
		$data = Events::model()->findByPk($nom_id);

		$this->render('reg_sukses', array(
						'data' => $data,
					));
	}

	public function actionViewmail()
	{
		$q_event = Events::model()->findByPk(15);
		$emails = 'gobes03@mailinator.com';
		$model = EventsReg::model()->find('email = :email and event_id = :evn_id', array(':email'=>$emails, ':evn_id'=> $q_event->id));

		$frm_active = Yii::app()->db->createCommand('
			SELECT a.*, b.active 
			FROM tb_events_form a 
			right join tb_event_part b on a.id = b.form_id 
			where b.events_id = '.$q_event->id.' and b.active = 1 order by id asc')
		->queryAll();

		// config email
		$messaged = $this->renderPartial('//mail/events_schedule',array(
			'model'=>$model,
			'event_data' => $q_event,
			'frm_active' => $frm_active,
		),TRUE);

		echo "<pre>";
		print_r($messaged);
		exit;

	}

	// checking kode
	public function actionSeminar()
	{
		$this->layout='//layouts/column_event';
		$this->pageTitle = 'Event Registration '. $this->pageTitle;
		
		$criteria = new CDbCriteria;
		$criteria->order = 't.id DESC';
		$criteria->limit = 1;
		$event_lasts = Events::model()->find($criteria);

		$error = false;
		if (isset($_GET['kode']) && $_GET['kode']) {
			$kodes = $_GET['kode'];
			$data = EventsReg::model()->find('register_code = :kode', array(':kode'=> $kodes));
			$parent_Event = Events::model()->find('id = :ids', array(':ids'=> $data->event_id));

			if ($data === null) {
				$error = true;
			}else{
				$error = false;
				$this->render('kode_scan', array(
					'data' => $data,
					'parent_Event' => $parent_Event,
				));
			}
		}

		if (isset($_POST['email']) && $_POST['email']) {
			$kodes = $_POST['email'];
			$data = EventsReg::model()->find('email = :k_email order by id desc', array(':k_email'=> $kodes));
			$parent_Event = Events::model()->find('id = :ids', array(':ids'=> $data->event_id));

			if ($data === null) {
				$error = true;
			}else{
				$error = false;
				$this->render('kode_scan', array(
					'data' => $data,
					'parent_Event' => $parent_Event,
				));
			}
		}

		if ( ((!isset($_GET['kode']) and !isset($_POST['email'])) or $error == true)) {
			$this->render('hadir_gagal', array(
					'data' => false,
					'error' => $error,
					'parent_Event' => $event_lasts,
				));
		}
	}

	// mass process approve
	public function actionPresent_event()
	{
		$this->layout='//layouts/column_event';
		$criteria = new CDbCriteria;
		$criteria->order = 't.id DESC';
		$criteria->limit = 1;
		$event_active = Events::model()->find($criteria);
		
		$error_code = false;

		if (isset($_POST['EventsReg'])) {
			$emails = $_POST['EventsReg']['email'];
			$event_id = $_POST['EventsReg']['event_id'];
			
			$post_kode = '';
			foreach ($_POST['EventsReg']['verifyCode'] as $key_n => $values_n) {
				$post_kode .= trim($values_n);
			}	

			$data = EventsReg::model()->find('email = :email and event_id = :evn_id', array(':email'=> $emails, ':evn_id'=> $event_id));
			$parent_Event = Events::model()->find('id = :ids', array(':ids'=> $event_id));
			// verify code TRUE
			
			// var_dump($post_kode);
			// echo "<br>";
			// var_dump($parent_Event->kode_verify);
			// exit;
			
			if (intval($post_kode) == intval($parent_Event->kode_verify)) {
				if ($data !== null) {
						if (intval($data->hadir_sesi_1) == 1 && intval($data->hadir_sesi_2) == 1 && intval($data->hadir_sesi_3) != 1) {
							$data->hadir_sesi_3 = 1;
						}

						if (intval($data->hadir_sesi_1) == 1 && intval($data->hadir_sesi_2) != 1) {
							$data->hadir_sesi_2 = 1;
						}

						if (intval($data->hadir_sesi_1) != 1) {
							$data->hadir_sesi_1 = 1;
						}
						$data->save(false);

					$this->render('hadir_sukses', array(	
						'error_code' => $error_code,
						'data' => $data,
						'parent_Event' => $parent_Event,
					));
				}
			}else{
				$error_code = true;
				$this->render('kode_scan', array(	
					'error_code' => $error_code,
					'data' => $data,
					'parent_Event' => $parent_Event,
				));
			}
		}else{
			$this->redirect(array('/event/seminar'));
		}

	}

		// mass scan result
	public function actionScan_result()
	{
		$this->pageTitle = 'Mass Registration - '.$this->pageTitle;
		$this->layout='//layouts/column_event';

		if (isset($_GET['member_id'])) {
			$ids = $_GET['member_id'];
			
			$data = EventsReg::model()->findByPk($ids);

			$this->render('scan_result', array(	
				'data' => $data,
			));

		}else{
			$this->redirect(array('seminar'));
		}
	}
}