<section class="covertop-products mb-3">
    <div class="prelatife container">
        <div class="row inners_section">
            <div class="col-md-20 my-auto align-middle py-5">
                <div class="description_text py-5">
                    <h3>Event Registration</h3>
                    <p>Please register for the training events / seminars you want to attend below.</p>
                </div>
            </div>
            <div class="col-md-40">
                <div class="banner_picture">
                    <img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['event_hero_image'] ?>" alt="" class="img img-fluid"></div>
            </div>
        </div>
        <div class="clear clearfix"></div>
    </div>    
</section>

<section class="breadcrumb-det">
    <div class="prelative container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/event')); ?>">Event Registration</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo ucwords(str_replace('-', ' ', $_GET['slug'])) ?></li>
            </ol>
        </nav>
    </div>
</section>


<section class="back-white">
    <div class="prelative container">
        
        <div class="py-2 my-1"></div>
        <!-- start content -->
        <div class="blocks-widget_choose_events">
            <div class="blocks_detail_event mb-4 pb-2 text-center maw600 d-block mx-auto">
                <h2 class="m-0 mb-0"><?php echo strtoupper(ucwords($data->name)) ?></h2>
                <div class="py-1"></div>
                <p><b><?php echo date("d F Y", strtotime($data->tgl_event)); ?>.</b> <?php echo $data->location ?></p>
                <div class="py-2"></div>
                <p class="f_zoom z2"><strong><?php echo $this->setting['event_titles'] ?></strong></p>
                <p class="f_zoom"><strong><?php echo $data->content_sucess; ?><?php //echo $this->setting['event_contents'] ?></strong></p>
            </div>
            <div class="py-1"></div>

            <div class="clear clearfix"></div>
        </div>
        <!-- end content -->
        <div class="py-2"></div>

        <div class="clear"></div>
    </div>
</section>

<style type="text/css">
    body, html{
        height: auto;
    }
    p.f_zoom{
    	font-size: 15px !important;
    }
    p.f_zoom.z2{
    	font-size: 17px !important;
    }
    .maw600 {
	    max-width: 685px !important;
	}
</style>