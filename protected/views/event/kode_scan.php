<section class="outers_block_nevents">
	<div class="prelatife container py-3">
		<div class="maw600 d-block mx-auto text-center">
			<h5>EVENT:</h5>
			<h3 class="titles"><?php echo $parent_Event->name; ?></h3>
			<p><strong><?php echo date('d F Y', strtotime($parent_Event->tgl_event)) ?>.</strong> <?php echo $parent_Event->location ?></p>
		</div>
	</div>
</section>

<section class="sections_def_event back-white py-5">
	<div class="prelatife container py-4">
		<div class="d-block mx-auto text-center content-verify">
			<h5>Verifikasi Kode</h5>
			
			<div class="boxs-form my-5 inlines-form">
				<?php if ($error_code): ?>
					<div class="alert alert-danger d-block mw480 mx-auto" role="alert">
						<ul>
							<li>Sorry, Your Code is Wrong</li>
						</ul>
					</div>
				<?php endif ?>
				<form class="justify-content-center digit-group" autocomplete="off" method="POST" action="<?php echo CHtml::normalizeUrl(array('/event/present_event')); ?>">
					  <input type="number" name="EventsReg[verifyCode][]" maxlength="1" class="form-control mr-sm-2 kode" required="" maxlength="1" id="codeBox1" onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)">
					  <input type="number" name="EventsReg[verifyCode][]" maxlength="1" class="form-control mr-sm-2 kode" required="" maxlength="1" id="codeBox2" onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(2)">
					  <input type="number" name="EventsReg[verifyCode][]" maxlength="1" class="form-control mr-sm-2 kode" required="" maxlength="1" id="codeBox3" onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(3)">
					  <input type="number" name="EventsReg[verifyCode][]" maxlength="1" class="form-control mr-sm-2 kode" required="" maxlength="1" id="codeBox4" onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(4)">
					  <?php /*<input type="number" name="EventsReg[verifyCode][]" maxlength="1" class="form-control mr-sm-2 kode" required="" maxlength="1" id="codeBox5" onkeyup="onKeyUpEvent(5, event)" onfocus="onFocusEvent(5)">
					  <input type="number" name="EventsReg[verifyCode][]" maxlength="1" class="form-control mr-sm-2 kode" required="" maxlength="1" id="codeBox6" onkeyup="onKeyUpEvent(6, event)" onfocus="onFocusEvent(6)">
					  */ ?>
					  
					  <input type="hidden" name="EventsReg[id]" value="<?php echo $data->id; ?>">
					  <input type="hidden" name="EventsReg[email]" value="<?php echo $data->email; ?>">
					  <input type="hidden" name="EventsReg[event_id]" value="<?php echo $data->event_id; ?>">
					  <div class="clear clearfix d-block">
						  <div class="py-2 my-1"></div>
					  </div>
					  <button type="submit" class="btn btn-light code_verify">VERIFY</button>
					</form>
			</div>

		</div>
	</div>
</section>

<script type="text/javascript">
	function getCodeBoxElement(index) {
	  return document.getElementById('codeBox' + index);
	}
	function onKeyUpEvent(index, event) {
	  const eventCode = event.which || event.keyCode;
	  if (getCodeBoxElement(index).value.length === 1) {
		 if (index !== 4) {
			getCodeBoxElement(index+ 1).focus();
		 } else {
			getCodeBoxElement(index).blur();
			// Submit code
			console.log('submit code ');
		 }
	  }
	  if (eventCode === 8 && index !== 1) {
		 getCodeBoxElement(index - 1).focus();
	  }
	}
	function onFocusEvent(index) {
	  for (item = 1; item < index; item++) {
		 const currentElement = getCodeBoxElement(item);
		 if (!currentElement.value) {
			  currentElement.focus();
			  break;
		 }
	  }
	}
</script>

<?php /*
<!-- <section class="sections_def_event back-grey1 py-5">
	<div class="prelatife container py-4">
		<div class="d-block mx-auto text-center content-verify">
			<h5>KEHADIRAN DALAM SESI EVENT</h5>
			<div class="py-3 my-2"></div>

			<div class="mw395 d-block mx-auto results_absents">
				<div class="row">
					<div class="col">
						<p><b>SESI 1</b></p>
						<div class="py-1"></div>
						<div class="boxs_white">
							<span><i class="fa fa-star"></i></span>
						</div>
					</div>
					<div class="col">
						<p><b>SESI 2</b></p>
						<div class="py-1"></div>
						<div class="boxs_white">
							<span><i class="fa fa-star"></i></span>
						</div>
					</div>
					<div class="col">
						<p><b>SESI 3</b></p>
						<div class="py-1"></div>
						<div class="boxs_white">
							<span><i class="fa fa-star"></i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section> -->
*/ ?>
<style type="text/css">
	body{
		background-color: #ededed;
	}

	.alert.alert-danger{
		max-width: 410px;
		font-size: 13px;
		text-align: left;
		margin-bottom: 1.6rem;
	}
	.alert.alert-danger ul{
		margin: 0;
	}
</style>