<section class="covertop-products mb-3">
    <div class="prelatife container">
        <div class="row inners_section">
            <div class="col-md-20 my-auto align-middle py-5">
                <div class="description_text py-5">
                    <h3>Event Registration</h3>
                    <p>Please register for the training events / seminars you want to attend below.</p>
                </div>
            </div>
            <div class="col-md-40">
                <div class="banner_picture">
                    <img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['event_hero_image'] ?>" alt="" class="img img-fluid"></div>
            </div>
        </div>
        <div class="clear clearfix"></div>
    </div>    
</section>

<section class="breadcrumb-det">
    <div class="prelative container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Event Registration</li>
            </ol>
        </nav>
    </div>
</section>


<section class="back-white">
    <div class="prelative container">
        
        <div class="py-1"></div>
        <!-- start content -->
        <div class="blocks-widget_choose_events">
            <div class="top-title mb-4 pb-2 text-center">
                <h3 class="m-0 mb-0">Choose Your Event Below</h3>
            </div>
            <div class="py-1"></div>
            <div class="lists_choose_event">
                <?php if ( count($data) > 0): ?>
                <div class="row">
                    <?php foreach ($data as $key => $value): ?>
                    <div class="col-md-30">
                        <div class="box_items p-3">
                            <h4><a class="<?php echo (intval($value->status) != 1)? 'disabled_link' : ''; ?>" href="<?php echo CHtml::normalizeUrl(array('/event/detail', 'id'=> $value->id, 'slug'=> Slug::Create($value->name) )); ?>"><?php echo strtoupper($value->name) ?></a></h4>
                            <p><b><?php echo date("d F Y", strtotime($value->tgl_event)); ?>.</b> <?php echo $value->location ?></p>
                            <div class="clear"></div>
                            <a class="<?php echo (intval($value->status) != 1)? 'disabled_link' : ''; ?>" href="<?php echo CHtml::normalizeUrl(array('/event/detail', 'id'=> $value->id, 'slug'=> Slug::Create($value->name))); ?>" class="btn btn-link p-0"><img src="<?php echo Yii::app()->baseUrl.'/asset/images/' ?>click.png" alt="" class="img-fluid"> &nbsp;&nbsp;Register</a>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
                <?php endif ?>
            </div>

            <div class="clear clearfix"></div>
        </div>
        <!-- end content -->
        <div class="py-3"></div>

        <div class="clear"></div>
    </div>
</section>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    $(function(){
        
        $('.disabled_link').on('click', function(e){
            e.preventDefault();
            swal("Event has been Closed!.");
            return false;
        });

    });
</script>
<style type="text/css">
    .swal-footer {
        text-align: center !important;
    }
</style>