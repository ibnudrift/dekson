<section class="outers_block_nevents">
	<div class="prelatife container py-3">
		<div class="maw600 d-block mx-auto text-center">
			<h5>EVENT:</h5>
			<h3 class="titles"><?php echo $parent_Event->name; ?></h3>
			<p><strong><?php echo date('d F Y', strtotime($parent_Event->tgl_event)) ?>.</strong> <?php echo $parent_Event->location ?></p>
		</div>
	</div>
</section>

<section class="sections_def_event back-white py-5">
	<div class="prelatife container py-4">
		<div class="d-block mx-auto text-center content-verify">
			<h5>Verifikasi Gagal, Masukkan Email Address untuk Pengecekan</h5>
			
			<div class="boxs-form my-5">
				<?php if ($error): ?>
					<div class="alert alert-danger d-block mw480 mx-auto" role="alert">
						<ul>
							<?php if (isset($_GET['kode'])): ?>
								<li>Sorry, Your Code is wrong</li>
							<?php else: ?>
								<li>Sorry, Your Email is Wrong</li>
							<?php endif ?>
						</ul>
					</div>
				<?php endif ?>

				<form class="form-inline justify-content-center" method="POST" action="">
				  <input type="text" class="form-control mr-sm-2" name="email" placeholder="EMAIL">
				  <button type="submit" class="btn btn-primary">SUBMIT</button>
				</form>
			</div>

			<p>Atau <a href="<?php echo CHtml::normalizeUrl(array('/event/index')); ?>">klik di sini</a> untuk registrasi ulang.</p>
		</div>
	</div>
</section>

<style type="text/css">
	body{
		background-color: #ededed;
	}

	.alert.alert-danger{
		max-width: 410px;
		font-size: 13px;
		text-align: left;
		margin-bottom: 1.6rem;
	}
	.alert.alert-danger ul{
		margin: 0;
	}
</style>