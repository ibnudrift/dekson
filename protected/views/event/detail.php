<section class="covertop-products mb-3">
    <div class="prelatife container">
        <div class="row inners_section">
            <div class="col-md-20 my-auto align-middle py-5">
                <div class="description_text py-5">
                    <h3>Event Registration</h3> 
                    <p>Please register for the training events / seminars you want to attend below.</p>
                </div>
            </div>
            <div class="col-md-40">
                <div class="banner_picture">
                    <img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['event_hero_image'] ?>" alt="" class="img img-fluid"></div>
            </div>
        </div>
        <div class="clear clearfix"></div>
    </div>    
</section>

<section class="breadcrumb-det">
    <div class="prelative container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/event')); ?>">Event Registration</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo ucwords(str_replace('-', ' ', $_GET['slug'])) ?></li>
            </ol>
        </nav>
    </div>
</section>


<section class="back-white">
    <div class="prelative container">
        
        <div class="py-2 my-1"></div>
        <!-- start content -->
        <div class="blocks-widget_choose_events">
            <div class="blocks_detail_event mb-4 pb-2 text-center maw600 d-block mx-auto">
                <h2 class="m-0 mb-0"><?php echo strtoupper(ucwords($data->name)) ?></h2>
                <div class="py-1"></div>
                <p><b><?php echo date("d F Y", strtotime($data->tgl_event)); ?>.</b> <?php echo $data->location ?></p>
                <?php if ($data->description): ?>
                <p><?php echo nl2br($data->description) ?></p>
                <?php endif ?>
            </div>
            <div class="py-1"></div>

            <div class="block_form-regis_events">
                <p>Daftarkan diri anda untuk mengikuti seminar.</p>
                <div class="py-4"></div>

                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                    'enableAjaxValidation'=>false,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>false,
                    ),
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data', 
                        'class' => 'form_reg',
                    ),
                )); ?>
                <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger', 'role'=>'alert')); ?>
                <?php if(Yii::app()->user->hasFlash('success')): ?>
                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
                        'alerts'=>array('success'),
                    )); ?>
                <?php endif; ?>

                <?php $model->event_id = $data->id; ?>
                <?php echo $form->hiddenField($model, 'event_id', array()); ?>

                <?php if (isset($frm_active)): ?>
                    <?php foreach ($frm_active as $key => $value): ?>
                        <?php if ($value['locks'] == "1"): ?>
                        <div class="row">
                            <div class="col-md-20"><label><?php echo $value['label'] ?></label></div>
                            <div class="col">
                                <?php if ($value['types'] == 'text'): ?>
                                    <?php echo $form->textField($model, $value['name'], array('class'=>'form-control', 'required'=>'required')); ?>
                                <?php elseif($value['types'] == 'datepicker'): ?>
                                    <?php echo $form->textField($model, $value['name'], array('class'=>'form-control datepicker', 'required'=>'required')); ?>
                                <?php elseif($value['types'] == 'textarea'): ?>
                                    <?php echo $form->textArea($model, $value['name'], array('class'=>'form-control datepicker', 'required'=>'required')); ?>
                                <?php elseif($value['types'] == 'dropdown'): ?>
                                    <?php if ($value['name'] == 'hobi'): ?>
                                        <?php 
                                        $dt_hobi = [
                                        'Sepeda' => 'Sepeda',
                                        'Travelling' => 'Travelling',
                                        'Kuliner' => 'Kuliner',
                                        'Reading' => 'Reading',
                                        'Olahraga' => 'Olahraga',
                                        'Arung Jeram' => 'Arung Jeram',
                                        'Hiking' => 'Hiking',
                                        'Movie' => 'Movie',
                                        'Memancing' => 'Memancing',
                                        'Fotografi' => 'Fotografi',
                                        'Lain-lain' => 'Lain-lain',
                                       ];
                                        ?>
                                        <?php echo $form->DropDownList($model, $value['name'], $dt_hobi, array('class'=>'form-control sel_hobi', 'required'=>'required')); ?>
                                        <?php echo $form->textField($model, 'hobi_lain', array('class'=>'form-control d-none vn_hobi_lain mt-3')); ?>
                                    <?php else: ?>
                                        <?php echo $form->DropDownList($model, $value['name'], array('class'=>'form-control', 'required'=>'required')); ?>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                        </div>
                        <?php elseif($value['locks'] == "0"): ?>
                        <?php 
                        $valn = isset($_POST['EventsReg']['datas'][$value['name']])? $_POST['EventsReg']['datas'][$value['name']] : '';
                        ?>
                        <div class="row">
                            <div class="col-md-20"><label><?php echo $value['label'] ?></label></div>
                            <div class="col">
                                <input class="form-control" required="required" name="EventsReg[datas][<?php echo $value['name'] ?>]" id="" value="<?php echo $valn ?>" type="text">
                            </div>
                        </div>
                        <?php endif ?>
                        <div class="py-2"></div>
                    <?php endforeach ?>
                                    
                <div class="py-2 my-1"></div>
                <div class="row">
                    <div class="col">
                        <div class="g-recaptcha" data-sitekey="6Le4sLQZAAAAAIlLULR9l-WCxixflYzTpNIxJoXu"></div>
                    </div>
                    <div class="col">
                        <div class="float-right">
                        <button type="submit" class="btn btn-link"></button>                    
                        </div>
                    </div>
                </div>
                <?php else: ?>
                    <div class="py-4">
                        <h3 class="text-center">Pendaftaran di tutup</h3>
                    </div>
                <?php endif ?>

                <?php $this->endWidget(); ?>

                <div class="claer"></div>
            </div>


            <div class="clear clearfix"></div>
        </div>
        <!-- end content -->
        <div class="py-4"></div>

        <div class="clear"></div>
    </div>
</section>

<script src='https://www.google.com/recaptcha/api.js'></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
$( function() {
      
      $( ".datepicker" ).datepicker({
        changeMonth: true, 
        changeYear: true, 
        dateFormat: "yy-mm-dd",
        yearRange : '-80:+0'
      });

      // $('.form_reg')
      $('select.sel_hobi').change(function(){
        if ( $(this).val() == 'Lain-lain'){
            $('input.vn_hobi_lain').removeClass('d-none').attr('required', 'required');
        }else{
            $('input.vn_hobi_lain').addClass('d-none').removeAttr('required');
        }
      });

});
</script>
<style type="text/css">
    body, html{
        height: auto;
    }
</style>