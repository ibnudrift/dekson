<section class="outers_block_nevents">
	<div class="prelatife container py-3">
		<div class="maw600 d-block mx-auto text-center">
			<h5>EVENT:</h5>
			<h3 class="titles"><?php echo $parent_Event->name; ?></h3>
			<p><strong><?php echo date('d F Y', strtotime($parent_Event->tgl_event)) ?>.</strong> <?php echo $parent_Event->location ?></p>
			<div class="py-3"></div>
			<div class="alert alert-light">
				<p>Undangan seminar zoom akan dikirim oleh panitia ke nomor Whatsapp atau email yang terdaftar. Jika belum menerima dalam waktu 1 hari sebelum seminar dimulai, harap menghubungi panitia.</p>
			</div>
		</div>
	</div>
</section>

<section class="sections_def_event back-white py-5">
	<div class="prelatife container py-4">
		<div class="d-block mx-auto text-center content-verify">
			<h5>Verifikasi Sukses</h5>
			<div class="py-2"></div>
			<div class="blocks_info_users pt-4">
				<p><?php echo $data->name; ?><br><?php echo $data->usia ?> TAHUN<br><?php echo $data->phone ?><br><?php echo $data->email ?><br><?php echo nl2br($data->address) ?>
				<br><?php echo $data->company ?></p>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="sections_def_event back-grey1 py-5">
	<div class="prelatife container py-4">
		<div class="d-block mx-auto text-center content-verify">
			<h5>KEHADIRAN DALAM SESI EVENT</h5>
			<div class="py-3 my-2"></div>

			<div class="mw395 d-block mx-auto results_absents">
				<div class="row">
					<div class="col">
						<p><b>SESI 1</b></p>
						<div class="py-1"></div>
						<div class="boxs_white">
							<?php if ($data->hadir_sesi_1 == "1"): ?>
							<span><i class="fa fa-star"></i></span>
							<?php endif ?>
						</div>
					</div>
					<div class="col">
						<p><b>SESI 2</b></p>
						<div class="py-1"></div>
						<div class="boxs_white">
							<?php if ($data->hadir_sesi_2 == "1"): ?>
								<span><i class="fa fa-star"></i></span>
							<?php else: ?>
								<span><i class="fa fa-minus"></i></span>
							<?php endif ?>
						</div>
					</div>
					<div class="col">
						<p><b>SESI 3</b></p>
						<div class="py-1"></div>
						<div class="boxs_white">
							<?php if ($data->hadir_sesi_3 == "1"): ?>
								<span><i class="fa fa-star"></i></span>
							<?php else: ?>
								<span><i class="fa fa-minus"></i></span>
							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>