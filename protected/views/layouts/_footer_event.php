<footer class="foot_events">
	<div class="prelatife container">
		<div class="row">
			<div class="col">
				<div class="lg_footers"><img src="<?php echo Yii::app()->baseUrl.'/asset/images/' ?>logo-fot.png" alt=""></div>
			</div>
			<div class="col-md-40">
				<div class="ts_copyrights">
					<p>Dekkson presents the high quality product range of architectural accessories - mechanism - electronics - and many more.</p>
					<p class="f2">&copy; COPYRIGHT 2019 - DEKKSON ARCHITECTURE ACCESSORIES. ALL RIGHTS RESERVED. WEBSITE DESIGN BY <a title="Website Design Surabaya" target="_blank" href="https://www.markdesign.net">MARKDESIGN</a>.</p>
				</div>
			</div>
			<div class="col">
				<div class="ft-socials">
					<ul class="list-inline text-right">
						<li class="list-inline-item">
							<a target="_blank" href="<?php echo $this->setting['url_instagram'] ?>"><i class="fa fa-instagram"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="<?php echo $this->setting['url_facebook'] ?>"><i class="fa fa-facebook"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="<?php echo $this->setting['url_youtube'] ?>"><i class="fa fa-youtube"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>