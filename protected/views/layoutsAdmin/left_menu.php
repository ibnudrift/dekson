<div class="leftmenu">        
    <ul class="nav nav-tabs nav-stacked">
        <li class="nav-header">Navigation</li>
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>"><span class="fa fa-tag"></span> <?php echo Tt::t('admin', 'Products') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>">View Products</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/create')); ?>">Add Products</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/filtercat/index')); ?>">Categorys</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/brand/index')); ?>">Brand</a></li>
            </ul>
        </li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/brand/index')); ?>"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Brand') ?></a></li>

        <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/promo/index')); ?>"><span class="fa fa-fax"></span> <?php echo Tt::t('admin', 'Voucher Discount') ?></a></li> -->

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/order/index')); ?>"><span class="fa fa-fax"></span> <?php echo Tt::t('admin', 'Enquire Product') ?> (<?php echo OrOrder::model()->count('is_read = 0') ?>)</a></li>

        <li class="dropdown"><a href="#"><span class="fa fa-heart"></span> <?php echo Tt::t('admin', 'Slide') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>">View Slide</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/create')); ?>">Create Slide</a></li>
            </ul>
        </li>

        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Brochure') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/brosur/index')); ?>">View Brochure List</a></li>
            </ul>
        </li>    
        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Project Lists') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/gallery/index')); ?>">View Project List</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/tipeGallery/index')); ?>">View Type Project</a></li>
            </ul>
        </li>        

        <li>&nbsp;</li>
        <li class="dropdown">
            <a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Blog') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog')); ?>">Artikel / Blog List</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Events Module') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/events')); ?>">Event List</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/eventsForm')); ?>">Master List Form</a></li>
            </ul>
        </li>
        <li>&nbsp;</li>

        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Static Page') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/home')); ?>">Homepage</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/project')); ?>">Projects</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/product')); ?>">Products</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/brosur')); ?>">Brochure</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/showroom')); ?>">Showroom</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/event')); ?>">Event</a></li>
                </li>
            </ul>
        </li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/contact')); ?>"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Contact Us') ?></a></li>
        

        <li><a href="<?php echo CHtml::normalizeUrl(array('setting/index')); ?>"><span class="fa fa-cogs"></span> <?php echo Tt::t('admin', 'General Setting') ?></a>
             <!--  <ul>
                <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/admin/administrator/index')); ?>">Administrator Manager</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/language/index')); ?>">Language (Bahasa)</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/access_block/index')); ?>">Access Blok</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/contact/index')); ?>">Contact & Form Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/meta_page/index')); ?>">Default Meta Page</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/google_tools/index')); ?>">Google Tools</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/#/index')); ?>">Import/Export Product</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/purechat/index')); ?>">Integrasi PureChat</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/invoice_setting/index')); ?>">Invoice Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/logo_setting/index')); ?>">Logo Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/mail_setting/index')); ?>">Mail Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/mailchimp/index')); ?>">MailChimp</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/marketplace/index')); ?>">Market Place</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/mobile_text/index')); ?>">Mobile Text Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/payment/index')); ?>">Payment Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/shipping/index')); ?>">Pengaturan Shipping</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/popOut/index')); ?>">Setting PopOut</a></li>
            </ul> -->
        </li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/home/logout')); ?>"><span class="fa fa fa-sign-out"></span> Logout</a></li>
    </ul>
</div><!--leftmenu-->
