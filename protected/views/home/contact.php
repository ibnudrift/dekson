<section class="breadcrumb-det">
    <div class="prelative container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
            </ol>
        </nav>
    </div>
</section>

<section class="contact-sec-1">
  <div class="prelative container">
    <div class="row no-gutters">
      <div class="col-md-20">
        <div class="images"><img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['contact_picture'] ?>" alt=""></div>
      </div>
      <div class="col-md-40">
        <div class="box-content">
          <div class="caption">
           <?php echo $this->setting['contact_content'] ?>
          </div>
          <div class="headquarter">
            <h5>Headquarter</h5>
            <p><?php echo $this->setting['contact_address'] ?></p>
          </div>
          <div class="contact">
            <h5>Email.</h5>
            <p><?php echo $this->setting['email'] ?> or </p>
            <h5>Whatsapp </h5>
            <?php
            $nums_wa = str_replace('08', '628',  str_replace(' ', '', $this->setting['contact_wa']) );
            ?>
            <a href="https://wa.me/<?php echo $nums_wa ?>"><p><?php echo $this->setting['contact_wa'] ?> (Click to chat)</p></a>
          </div>
          <div class="telephone">
            <h5>Telephone. </h5>
            <p><?php echo $this->setting['contact_phone'] ?></p>
          </div>

          <div class="hr-garis"></div>

          <div class="inquiry">
              <h4>Dekkson Inquiry Form</h4>
            </div>
          <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                    // 'type'=>'horizontal',
                    'enableAjaxValidation'=>false,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>false,
                    ),
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                    ),
                )); ?>
                <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger', 'role'=>'alert')); ?>
                <?php if(Yii::app()->user->hasFlash('success')): ?>
                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
                        'alerts'=>array('success'),
                    )); ?>
                <?php endif; ?>
            <div class="form-row">
              <div class="form-group col-md-20">
                <?php echo $form->textField($model, 'name', array('class'=>'form-control', 'placeholder'=>'Your Name')); ?>
              </div>
              <div class="form-group col-md-20">
                <?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'placeholder'=>'Your Mobile Phone')); ?>
              </div>
              <div class="form-group col-md-20">
                <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'placeholder'=>'Your Email')); ?>
              </div>
              <div class="form-group col-md-40">
                <?php echo $form->textArea($model, 'body', array('class'=>'form-control', 'placeholder'=>'Your Message')); ?>
              </div>
              <div class="form-group col-md-20">
                <div class="submit">
                  <button type="submit" class="btn btn-link btns_submits_contact"><img src="<?php echo Yii::app()->baseUrl.'/asset/images/click.png'; ?>" alt="">&nbsp; Submit</button>
                </div>
              </div>
              <div class="form-group">
                <div class="g-recaptcha" data-sitekey="6Le4sLQZAAAAAIlLULR9l-WCxixflYzTpNIxJoXu"></div>
              </div>
            </div>
          <?php $this->endWidget(); ?>
  
          <div class="opening">
            <p>*We are operting on GMT +7 operation hours. All inquiries are valueable to us and will be responded accordingly.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script src='https://www.google.com/recaptcha/api.js'></script>