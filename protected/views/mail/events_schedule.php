<?php
$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
$url = Yii::app()->request->hostInfo;
?>
<div style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;min-height:100%;font-size:14px;color:#404040;margin:0;padding:0" bgcolor="#FFFFFF">
    <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0">
                </td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0">

                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;padding:0;border:0 solid #e7e7e7">
                        <table bgcolor="#fff" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td bgcolor="#706B5F" height="4" style="background-color:#B4B4B4!important;line-height:4px;font-size:4px;margin:0;padding:0">&nbsp;
                                    </td>
                                    <td bgcolor="#d50f25" height="4" style="background-color:#959595!important;line-height:4px;font-size:4px;margin:0;padding:0">&nbsp;
                                    </td>
                                    <td bgcolor="#3369E8" height="4" style="background-color:#000000!important;line-height:4px;font-size:4px;margin:0;padding:0">&nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td style="margin:0;padding:0">
                </td>
            </tr>
        </tbody>
    </table>
    <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0">
                </td>
                <td bgcolor="#FFFFFF" style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0">
                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;padding:30px 15px;border:1px solid #e7e7e7">
                        <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <h5 style="line-height:24px;color:#000;font-weight:900;font-size:16px;margin:0 0 20px;padding:0">We have recorded your registration event. Thank you for your participation.</h5>
                                        <p style="margin: 0 0 10px; font-size: 13px;"><?php echo nl2br(htmlspecialchars($event_data->content_email)); ?></p>
                                        <hr>
                                        <p style="line-height: 19px; color: #151515; font-weight: 400; font-size: 15px; margin: 0 0 20px; padding: 0;">
                                            <strong><?php echo ucwords(strtolower($event_data->name)) ?></strong><br>
                                             <?php echo nl2br(ucwords(strtolower($event_data->location))); ?> <br>
                                             <?php echo date("d F Y", strtotime($event_data->tgl_event)); ?>
                                        </p>

                                        <p style="line-height: 24px; color: #999; font-weight: 400; font-size: 14px; margin: 0 0 20px; padding: 0; max-width: 160px; background: #e4e4e4; padding: 0.5rem;">
                                            <img src="<?php echo $model->url_qrcode; ?>" alt="Events Registration" style="display: block !important;max-width: 100%;">
                                        </p>

                                        <div style="margin:0 0 20px;padding:0">

                                            <?php foreach ($frm_active as $key => $value): ?>
                                            <?php if ($value['locks'] == "1"): ?>
                                            <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0">
                                                <tbody style="margin:0;padding:0">
                                                    <tr style="margin:0;padding:0">
                                                        <td valign="top" style="width:25%;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0"><?php echo $value['label'] ?>:
                                                        </td>
                                                        <td valign="top" style="font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0"><?php echo $model->{$value['name']} ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0">
                                            </div>
                                            <?php endif ?>

                                            <?php if ($value['locks'] == "0"): ?>
                                                <?php 
                                                $data_m = unserialize($model->datas);
                                                ?>
                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0">
                                                <tbody style="margin:0;padding:0">
                                                    <tr style="margin:0;padding:0">
                                                        <td valign="top" style="width:25%;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0"><?php echo ucwords($value['label']) ?>:
                                                        </td>
                                                        <td valign="top" style="font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0"><?php echo $data_m[$value['name']] ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0">
                                            </div>
                                            <?php endif ?>

                                            <?php endforeach ?>

                                            <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0">
                                                <tbody style="margin:0;padding:0">
                                                    <tr style="margin:0;padding:0">
                                                        <td valign="top" style="width:25%;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Nomor:
                                                        </td>
                                                        <td valign="top" style="font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0"><?php echo $model->nomer_peserta ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0">
                                            </div>

                                            <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0">
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td style="margin:0;padding:0">
                </td>
            </tr>
        </tbody>
    </table>
    <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;clear:both!important;background-color:transparent;margin:0 0 60px;padding:0;">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0">
                </td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0">

                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;padding:20px 15px;border-color:#e7e7e7;border-style:solid;border-width:0 1px 1px">
                        <table bgcolor="transparent" width="100%" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0">
                            <tbody style="margin:0;padding:0">
                                <tr style="margin:0;padding:0">
                                    <td valign="top" style="margin:0;padding:0 10px 0 0;width:75%">
                                        <span style="font-size:12px;margin-bottom:6px;display:inline-block">
                                            <b><?php echo Yii::t('mail', 'Contact') ?> <span class="lG"><?php echo Yii::app()->name; ?></span></b> <br>
                                            Phone: <?php echo $event_data->event_phone ?> <br>
                                            Email: <a href="mailto:<?php echo $event_data->event_email ?>"><?php echo $event_data->event_email ?></a> <br>
                                        </span> 
                                    </td>
                                    <td valign="top" style="margin:0;padding:0">
                                        <?php /*
                                        <span style="font-size:12px;margin-bottom:6px;display:inline-block"><?php echo Yii::t('mail', 'Ikuti Kami') ?></span>
                                        <div style="text-align:left">
                                            <a target="_blank" style="color:#008000;display:inline-block" href="<?php echo $this->setting['url_facebook'] ?>"><img style="border:0;min-height:auto;max-width:100%;outline:0" alt="Facebook" src="<?php echo $baseUrl ?>/asset/images/icon-facebook.png"></a>
                                            <a target="_blank" style="color:#008000;display:inline-block" href="<?php echo $this->setting['url_instagram'] ?>"><img style="border:0;min-height:auto;max-width:100%;outline:0" alt="Instagram" src="<?php echo $baseUrl ?>/asset/images/icon-instagram.png"></a>
                                        </div>
                                        */ ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td style="margin:0;padding:0">
                </td>
            </tr>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0">
                </td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0">
                    <div style="max-width:600px;display:block;border-collapse:collapse;background-color:#dadada;margin:0 auto;padding:20px 15px;border-color:#dadada;border-style:solid;border-width:0 1px 1px">
                        <table bgcolor="transparent" width="100%" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0">
                            <tbody style="margin:0;padding:0">
                                <tr style="margin:0;padding:0">
                                    <td valign="middle" style="margin:0;padding:0;width:53%">
                                        <p style="color:#000;font-size:10px;line-height:150%;font-weight:normal;margin:0px;padding:0px"><?php echo Yii::t('mail', 'If you need help, use the page') ?> <a target="_blank" style="color:#000;text-decoration:none;margin:0;padding:0" href="<?php echo $url.CHtml::normalizeUrl(array('/home/contact')); ?>"><?php echo Yii::t('mail', 'Our Contact') ?></a>.<br style="margin:0;padding:0"><?php echo date('Y') ?> &copy;

                                            <span class="lG"><?php echo Yii::app()->name; ?></span></p>
                                    </td>
                                    <td valign="middle" style="width:40%">
                                        <div style="text-align:right"><img alt="<?php echo Yii::app()->name; ?>" style="max-width: 105px; display: block; float: right;" src="<?php echo $baseUrl ?>/asset/images/logo-fot.png">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td style="margin:0;padding:0">
                </td>
            </tr>
        </tbody>
    </table>
</div>